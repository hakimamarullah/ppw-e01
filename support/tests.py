from django.test import TestCase, Client
from support.models import Support
from django.apps import apps
from support.apps import SupportConfig

# Create your tests here.

class SupportConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(SupportConfig.name, 'support')
        self.assertEqual(apps.get_app_config('support').name, 'support')

class SupportTestCase(TestCase):
    def test_url_status_200(self):
        response = Client().get('/formulirsupport/')
        self.assertEqual(200, response.status_code)
        response = Client().get('/landingsupport/')
        self.assertEqual(200, response.status_code)

    def test_template_formulir_support(self):
       response = Client().get('/formulirsupport/')
       self.assertTemplateUsed(response, 'formulirsupport.html')

    def test_template_landing_support(self):
       response = Client().get('/landingsupport/')
       self.assertTemplateUsed(response, 'landingsupport.html')
    
    def test_view_formulir_support(self):
        response = Client().get('/formulirsupport/')
        isi_html_respon = response.content.decode ('utf8')
        self.assertIn("Kirimkan", isi_html_respon)
    
    def test_form_formulir_support(self):
        Client().post('/formulirsupport/', data={'email' : 'ayuendri@gmail.com', 'pesan' : 'Saya ingin mengetahui hasil pemeriksanaan'})
        jumlah = Support.objects.filter(email='ayuendri@gmail.com').count()
        self.assertEqual(jumlah, 1)

    def test_string_representation_matkul(self):
        object_support = Support(email="unit_test@yahoo.com")
        self.assertEqual(str(object_support), object_support.email)

    def test_view_formulir_kegiatan(self):
        response = Client().get('/landingsupport/')
        isi_html_respon = response.content.decode ('utf8')
        self.assertIn("Pesan Anda Telah Terkirim", isi_html_respon)
        self.assertIn("Terima kasih telah mengirim pesan kepada kami! Kami akan segera mengirimkan jawaban melalui email Anda", isi_html_respon)
        self.assertIn("Kirim Pesan Lagi", isi_html_respon)
        self.assertIn("Balik Ke Halaman Utama", isi_html_respon)