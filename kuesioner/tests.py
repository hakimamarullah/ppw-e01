from django.test import TestCase, Client
from .models import *


# Create your tests here.
class KuesionerTes(TestCase):
    def test_model_kuesioner(self):
        Larissa = Kuesioner.objects.create(Nama="Larissa", Pertanyaan1=True, Pertanyaan2=True, Pertanyaan3=True, Pertanyaan4=True, Pertanyaan5=True, Pertanyaan6=True)
        self.assertEqual(str(Larissa), "Larissa")
        self.assertEqual(Larissa.hitung_nilai, 14)

    def test_kuesioner_form(self):
        response = Client().get('/kuesioner/')
        self.assertTemplateUsed(response, 'kuesioner.html')
        self.assertEqual(response.status_code, 200)
        cnt = Kuesioner.objects.all().count()
        Posting = Client().post('/kuesioner/', data={'Nama':'Adam Syauqi', 'Pertanyaan1':'True', 'Pertanyaan2':'True', 'Pertanyaan3':'True', 'Pertanyaan4':'True', 'Pertanyaan5':'True', 'Pertanyaan6':'True'})
        self.assertEqual(Posting.status_code, 302)
        Auki = Kuesioner.objects.get(id=1)
        self.assertEqual(str(Auki), "Adam Syauqi")
        self.assertEqual(Auki.hitung_nilai, 14)
        self.assertEqual(Kuesioner.objects.all().count(), cnt+1)

    def test_list_kuesioner(self):
        response = Client().get('/listkuesioner/')
        self.assertTemplateUsed(response, 'list_kuesioner.html')
        self.assertEqual(response.status_code, 200)

    def test_hasil_kuesioner(self):
        Posting = Client().post('/kuesioner/', data={'Nama':'Pekora', 'Pertanyaan1':'True', 'Pertanyaan2':'True', 'Pertanyaan3':'True', 'Pertanyaan4':'True', 'Pertanyaan5':'True', 'Pertanyaan6':'True'})
        Pekora = Kuesioner.objects.get(id=1)
        Akses = Client().get('/hasilkuesioner/' + str(Pekora.id) + '/')
        self.assertTemplateUsed(Akses, 'hasil_kuesioner.html')